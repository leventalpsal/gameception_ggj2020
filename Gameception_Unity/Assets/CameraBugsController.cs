﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBugsController : MonoBehaviour
{
    public GameObject normalCamera;
    public GameObject ditheringCamera;
    public GameObject pixelCamera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Bugs(BugEnums bugEnum)
    {
        if (bugEnum == BugEnums.Pixelation)
        {
            normalCamera.SetActive(false);
            ditheringCamera.SetActive(false);
            pixelCamera.SetActive(true);
        }
        else if (bugEnum == BugEnums.Dithering)
        {
            normalCamera.SetActive(false);
            ditheringCamera.SetActive(true);
            pixelCamera.SetActive(false);
        }
    }

    public void Fixes(BugEnums bugEnum)
    {
        if (bugEnum == BugEnums.Pixelation)
        {
            normalCamera.SetActive(true);
            ditheringCamera.SetActive(false);
            pixelCamera.SetActive(false);
        }
        else if (bugEnum == BugEnums.Dithering)
        {
            normalCamera.SetActive(true);
            ditheringCamera.SetActive(false);
            pixelCamera.SetActive(false);
        }
    }
}
