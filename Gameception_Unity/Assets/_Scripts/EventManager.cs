﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BugEvent : UnityEvent<BugEnums>
{
}

public class FixAction : UnityEvent<BugEnums>
{
}

public class FixHardwareEvent : UnityEvent<BugEnums>
{
}

public class EventManager : MonoBehaviour
{
    private ShipController shipController;
    private CameraBugsController cameraController;
    private EnemyController[] enemies;

    public List<BugEnums> activeSoftwareBugs = new List<BugEnums>();
    public List<BugEnums> activeHardwareBugs = new List<BugEnums>();

    public BugEvent softwareBugEvent;
    public BugEvent hardwareBugEvent;
    public UnityEvent fixSoftwareEvent;
    public UnityEvent fixHardwareEvent;
    private FixAction fixAction;


    void Start()
    {
        shipController = FindObjectOfType<ShipController>();
        enemies = FindObjectsOfType<EnemyController>();
        cameraController = FindObjectOfType<CameraBugsController>();

        if (softwareBugEvent == null)
        {
            softwareBugEvent = new BugEvent();
            hardwareBugEvent = new BugEvent();
            fixSoftwareEvent = new UnityEvent();
            fixAction = new FixAction();
            fixHardwareEvent = new UnityEvent();
        }

        softwareBugEvent.AddListener(shipController.Bugs);

        for (int i = 0; i < enemies.Length; i++)
        {
            softwareBugEvent.AddListener(enemies[i].Bugs);
        }

        softwareBugEvent.AddListener(cameraController.Bugs);

        hardwareBugEvent.AddListener(shipController.Bugs);

        for (int i = 0; i < enemies.Length; i++)
        {
            hardwareBugEvent.AddListener(enemies[i].Bugs);
        }

        hardwareBugEvent.AddListener(cameraController.Bugs);

        fixAction.AddListener(shipController.Fixes);

        for (int i = 0; i < enemies.Length; i++)
        {
            fixAction.AddListener(enemies[i].Fixes);
        }

        fixAction.AddListener(cameraController.Fixes);

        softwareBugEvent.AddListener(AddActiveSoftwareBug);
        hardwareBugEvent.AddListener(AddActiveHardwareBug);
        fixSoftwareEvent.AddListener(FixSoftwareBug);
        fixHardwareEvent.AddListener(FixHardwareBug);
    }

    // Update is called once per frame
    void Update()
    {
    }

    void AddActiveSoftwareBug(BugEnums bugEnum)
    {
        Debug.Log("bugEnum: " + bugEnum);
        activeSoftwareBugs.Add(bugEnum);
    }

    void AddActiveHardwareBug(BugEnums bugEnum)
    {
        activeHardwareBugs.Add(bugEnum);
    }

    void FixSoftwareBug()
    {
        if (activeSoftwareBugs.Count > 0)
        {
            fixAction.Invoke(activeSoftwareBugs[activeSoftwareBugs.Count - 1]);
            activeSoftwareBugs.Remove(activeSoftwareBugs[activeSoftwareBugs.Count - 1]);
        }
    }

    void FixHardwareBug()
    {
        if (activeHardwareBugs.Count > 0)
        {
            fixAction.Invoke(activeHardwareBugs[activeHardwareBugs.Count - 1]);
            activeHardwareBugs.Remove(activeHardwareBugs[activeHardwareBugs.Count - 1]);
        }
    }

}

public enum BugEnums
{
    WrongFire,
    UselessFire,
    Pixelation,
    Dithering,
    Movement,
    FasterEnemies
}
