﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// v0.1 2017.07.08
/// </summary>
public static class ExtensionMethods {

    /*
    public static bool Contains(this Collider col, Vector3 point)
    {
        if (col.GetType() == typeof(TerrainCollider))
        {
            // terrain'in özelliklerini alacağız. 
            // Verilen pozisyonun terraindeki heightmap koordinatını bulup orada yüksekliği heightmap'in altında mı üstünde mi bakacağız...
            // altında ise return true;
            //Debug.Log("Terrain collider");
            GameObject terrainGo = col.gameObject;
            TerrainData terrainData = terrainGo.GetComponent<TerrainCollider>().terrainData;
            if (null == terrainData)
            {
                Debug.LogWarning("null == terrainData");
                return false;
            }


            int xRes = terrainData.heightmapWidth; // 513
            int yRes = terrainData.heightmapHeight; // 513
            //int res = terrainData.heightmapResolution; // 513 
            //int dRes = terrainData.detailResolution; // 1024

            // TODO check X - Z not just Y

            var mapCoordinate = new int[2];

            mapCoordinate = TerrainHelper.WorldPosToTerrainMap(point, terrainGo);
            Debug.Log("mapCoordinate : " + mapCoordinate[0] + " . " + mapCoordinate[1]);
            return false;
            //  Map Unit height (0-1) of point's footprint on terrain. 
            float pointHeightMu = terrainData.GetHeight(mapCoordinate[0], mapCoordinate[1]); // +1 -1 gibi birşey olabilir
            float pointHeightMu2 = terrainData.GetInterpolatedHeight(mapCoordinate[0], mapCoordinate[1]); // ??
            float pointHeightMu3 = terrainGo.GetComponent<Terrain>().SampleHeight(point);
            float[,] areaT = terrainData.GetHeights(0, 0, xRes, yRes);
            //float[,] areaT = terrainData.GetHeights(mapCoordinate[0]-1, mapCoordinate[1]-1, 2, 2);
            float pointHeightMu4 = areaT[mapCoordinate[0], mapCoordinate[1]];
            //Debug.Log(" -1 " + pointHeightMu + " -2 " + pointHeightMu2 + " -3 " + pointHeightMu3 + " -4 " + pointHeightMu4);
            //  World Unit height (0-1) of point's footprint on terrain. 
            float pointHeightWu = TerrainHelper.TerrainHeightToWorldHeight(pointHeightMu, terrainGo);
            // Compare it with the actual height of point
            if(pointHeightWu > point.y)
            {
                // Point is below terrain, so it contains it.
                //Debug.Log("Contains True " + pointHeightWu + " - " + point.y);
                return true;
            } else
            {
                //Debug.Log("Contains false " + pointHeightWu + " - " + point.y);
                return false;
            }

        }

        // Terrain için farklı bir algorithma yazılabilir.
        if (col.ClosestPoint(point) == point)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    */


    /// <summary>
    /// Change X coordiante of transform position
    /// </summary>
    /// <param name="tf"></param>
    /// <param name="newPosX"></param>
    public static void SetPosX(this Transform tf, float newPosX, bool useLocalPosition = true)
    {
        if (useLocalPosition)
            tf.localPosition = new Vector3(newPosX, tf.localPosition.y, tf.localPosition.z);
        else
            tf.position = new Vector3(newPosX, tf.position.y, tf.position.z);
    }
    /// <summary>
    /// Change Y coordiante of transform position
    /// </summary>
    /// <param name="tf"></param>
    /// <param name="newPosY"></param>
    public static void SetPosY(this Transform tf, float newPosY, bool useLocalPosition = true)
    {
        if (useLocalPosition)
            tf.localPosition = new Vector3(tf.localPosition.x, newPosY, tf.localPosition.z);
        else
            tf.position = new Vector3(tf.position.x, newPosY, tf.position.z);
    }
    /// <summary>
    /// Change Z coordiante of transform position
    /// </summary>
    /// <param name="tf"></param>
    /// <param name="newPosZ"></param>
    public static void SetPosZ(this Transform tf, float newPosZ, bool useLocalPosition = true)
    {
        if(useLocalPosition)
            tf.localPosition = new Vector3(tf.localPosition.x, tf.localPosition.y, newPosZ);
        else
            tf.position = new Vector3(tf.position.x, tf.position.y, newPosZ);
    }

    /// <summary>
    /// Deletes all children of a GameObject
    /// </summary>
    /// <param name="go"></param>
    public static void DeleteAllChildren(this GameObject go)
    {
        for (int i = go.transform.childCount - 1; i >= 0; i--)
        {
            UnityEngine.Object.DestroyImmediate(go.transform.GetChild(i).gameObject);
        }
    }

    /// <summary>
    /// Apply torque only if Rpm limit isn't exceeded yet.
    /// </summary>
    /// <param name="wc"></param>
    /// <param name="motorTorque"></param>
    /// <param name="maxRpm"></param>
    /// <returns></returns>
    public static bool ApplyTorqueWithRpmLimit(this WheelCollider wc, float motorTorque, float maxRpm)
    {
        if(Mathf.Abs(wc.rpm) < maxRpm ||  Mathf.Sign(wc.rpm) != Mathf.Sign(motorTorque) )
        {
            wc.motorTorque = motorTorque;
            return true;
        }
        else
        {
            wc.motorTorque = 0;
            return false;
        }
    }

    public static int[] Shuffle (this int[] sortedArray)
    {
        List<int> sortedList = new List<int>(sortedArray);
        int[] randomArray = new int[sortedArray.Length];

        for (int i = 0; i < randomArray.Length; i++)
        {
            int randomIndex = UnityEngine.Random.Range(0, sortedList.Count);
            randomArray[i] = sortedList[randomIndex];
            sortedList.RemoveAt(randomIndex);
        }

        return randomArray;
        
    }

    public static float[] Shuffle(this float[] sortedArray)
    {
        List<float> sortedList = new List<float>(sortedArray);
        float[] randomArray = new float[sortedArray.Length];

        for (int i = 0; i < randomArray.Length; i++)
        {
            int randomIndex = UnityEngine.Random.Range(0, sortedList.Count);
            randomArray[i] = sortedList[randomIndex];
            sortedList.RemoveAt(randomIndex);
        }

        return randomArray;

    }
}
