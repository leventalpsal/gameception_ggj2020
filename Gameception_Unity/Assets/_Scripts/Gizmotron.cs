﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gizmotron : MonoBehaviour
{

    private static Dictionary<string, GizmoDraw> gizmoDrawList = new Dictionary<string, GizmoDraw>();

    public static void GizmoHelper(string key, Vector3 position, float scale, Color color, GizmoDraw.GizmoType gizmoType, float duration = -1f)
    {
        string type_key = gizmoType.ToString() + "_" + key;
        gizmoDrawList.Remove(type_key);
        gizmoDrawList.Add(type_key, new GizmoDraw(position, scale, color, gizmoType, duration));
    }

    public static void GizmoHelper(string key, Vector3 position, Vector3 toPosition, Color color, GizmoDraw.GizmoType gizmoType, float duration = -1f)
    {
        string type_key = gizmoType.ToString() + "_" + key;
        gizmoDrawList.Remove(type_key);
        gizmoDrawList.Add(type_key, new GizmoDraw(position, toPosition, color, gizmoType, duration));
    }

    void OnDrawGizmos()
    {
        Dictionary<string, GizmoDraw> newGizmoDrawList = new Dictionary<string, GizmoDraw>();
        foreach (KeyValuePair<string, GizmoDraw> item in gizmoDrawList)
        {
            Gizmos.color = item.Value.color;
            switch (item.Value.gizmoType)
            {
                case GizmoDraw.GizmoType.Line:
                    Gizmos.DrawLine(item.Value.position, item.Value.toPosition);
                    break;
                case GizmoDraw.GizmoType.Sphere:
                    Gizmos.DrawSphere(item.Value.position, item.Value.scale);
                    break;
                default:
                    break;
            }

            if (item.Value.duration == -1 || item.Value.endTime > Time.time)
            {
                newGizmoDrawList.Add(item.Key, item.Value);
            }
        }

        gizmoDrawList = newGizmoDrawList;
    }
}

public class GizmoDraw
{

    public enum GizmoType { Line, Sphere };
    public Vector3 position;
    public Vector3 toPosition;
    public float scale;
    public Color color;
    public GizmoType gizmoType;
    public float duration;
    public float endTime;

    public GizmoDraw(Vector3 position, float scale, Color color, GizmoType gizmoType, float duration)
    {
        this.position = position;
        this.scale = scale;
        this.color = color;
        this.gizmoType = gizmoType;
        this.duration = duration;
        endTime = Time.time + duration;
    }

    public GizmoDraw(Vector3 position, Vector3 toPosition, Color color, GizmoType gizmoType, float duration)
    {
        this.position = position;
        this.toPosition = toPosition;
        this.color = color;
        this.gizmoType = gizmoType;
        this.duration = duration;
        endTime = Time.time + duration;
    }




}