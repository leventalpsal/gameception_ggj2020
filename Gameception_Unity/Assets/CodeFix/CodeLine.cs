﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CodeLine : MonoBehaviour
{
    [SerializeField]
    private CodeLineType _codeLineType;
    public CodeLineType codeLineType { get { return _codeLineType; } }

    [SerializeField]
    private SpriteRenderer LineBaseSR;
    [SerializeField]
    private SpriteRenderer SlotSR;

    private bool isLineStartedBuggy = false;
    private bool isLineBuggy;
    public bool IsLineBuggy { get { return isLineBuggy; } }
    private CodeManager cm;

    [SerializeField] private GameObject CursorPrefab;
    private GameObject CursorGO;

    [SerializeField] private AudioSource keyboardAudio;

    // Start is called before the first frame update
    void Start()
    {
        cm = GetComponentInParent<CodeManager>();
        cm.LineAddedEvent.AddListener(CheckTopLimit);

        keyboardAudio = GetComponent<AudioSource>();

        if (_codeLineType == CodeLineType.Parentheses || _codeLineType == CodeLineType.Semicolon)
        {
            SetCodeLineSprite();
        }
        // SELECT Random Sprite from regarding group
        AlignSlotSprite();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SetCodeLineSprite()
    {
        LineBaseSR.sprite = cm.GetRandomCodeLineSprite();
    }

    void AlignSlotSprite()
    {
        SlotSR.transform.SetPosX( LineBaseSR.transform.localPosition.x + (LineBaseSR.bounds.extents.x * 2 / transform.lossyScale.x));
    }

    public void SetBug(bool isBuggy)
    {
        if (isBuggy)
        {
            isLineStartedBuggy = true;
        }
        else
        {
            SlotSR.enabled = true;
        }
        isLineBuggy = isBuggy;
    }

    private void CheckTopLimit()
    {
        if (transform.position.y > cm.TopBorderY)
        {
            // Going out of screen, will create a glitch in game if the line is wrong.
            if (isLineBuggy)
            {
                // Send a bug to Space Invaders
                Debug.Log("=== SEND BUG TO SINVS ===");
                cm.InitializeBug();
            }
            else if (isLineStartedBuggy)
            {
                // Line started buggy, then fixed
                cm.BugFixedCompiled();
            }
            cm.lineCompiledEvent.Invoke(this);
            Destroy(gameObject);
        }
    }

    public void AddLineEnder(CodeLineType addedCodeLineEnder)
    {
        SlotSR.sprite = cm.GetSpriteByCLT(addedCodeLineEnder);
        SlotSR.enabled = true;
        if (_codeLineType == addedCodeLineEnder)
        {
            SetBug(false);
            cm.ResetSelectedCodeLine();
            RemoveCursor();
            keyboardAudio.Play();
            Debug.Log("Fixed");
        }
    }

    public void AddCursor()
    {
        if (CursorGO == null)
        {
            CursorGO = Instantiate(CursorPrefab, transform);
            CursorGO.transform.localPosition = Vector3.zero;
            CursorGO.transform.SetPosX(SlotSR.transform.localPosition.x);
        }
    }

    public void RemoveCursor()
    {
        if (CursorGO != null)
        {
            Destroy(CursorGO);
            CursorGO = null;
        }
    }
}
