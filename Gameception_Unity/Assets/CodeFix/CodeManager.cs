﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum CodeLineType { Parentheses, CurlyBracesOpen, CurlyBracesClose, Semicolon }
public class LineCompiledEvent : UnityEvent<CodeLine> { }


public class CodeManager : MonoBehaviour
{
    public UnityEvent LineAddedEvent;
    public LineCompiledEvent lineCompiledEvent;

    [SerializeField] private GameObject CodeWrapper;
    [SerializeField] private GameObject TopBorder;
    public float TopBorderY { get { return TopBorder.transform.position.y; } }

    [SerializeField] private CodeLine clParentheses;
    [SerializeField] private CodeLine clCurlyOpen; // Indent ++
    [SerializeField] private CodeLine clCurlyClose; // Indent --
    [SerializeField] private CodeLine clSemicolon; // Normal code lines

    [SerializeField] private Sprite spParenthesesOpen;
    [SerializeField] private Sprite spParenthesesClose;
    [SerializeField] private Sprite spCurlyOpen;
    [SerializeField] private Sprite spCurlyClose;
    [SerializeField] private Sprite spSemicolon;

    private int tmpRnd;
    private CodeLineType prevCLT;
    private CodeLineType newCLT;
    private CodeLine newCodeLine;

    [SerializeField] private float indentationAmount; // how much indentation (x pos) will be applied.
    private int indentationLevel; // 0, 1 or 2 indentations

    [SerializeField] private float lineHeight; // 
    private int lineCount = 0; // 

    [SerializeField] private float NewLineInterval; // Time to spawn new line
    private float lineTimer = 0;

    private Vector3 InstantiatePosition;
    private EventManager eventManager;

    private CodeLine selectedCodeLine; // The code line we have highlighted for fix.
    private int BugTypeCount;

    [SerializeField] private List<Sprite> codeLineBaseSprites;

    // Start is called before the first frame update
    void Start()
    {
        eventManager = FindObjectOfType<EventManager>();
        InstantiatePosition = CodeWrapper.transform.position;
        lineCompiledEvent = new LineCompiledEvent();
        lineCompiledEvent.AddListener(HandleCodeLineCompiled);
        BugTypeCount = System.Enum.GetNames(typeof(BugEnums)).Length;
    }

    // Update is called once per frame
    void Update()
    {
        lineTimer += Time.deltaTime;
        if (lineTimer > NewLineInterval)
        {
            AddNewLine();
            LineAddedEvent.Invoke();
            lineTimer -= NewLineInterval;
        }

        CheckCodeClick();

        Lebug.Log("selectedCodeLine", selectedCodeLine);
    }

    #region Line Creation

    void AddNewLine()
    {
        prevCLT = newCLT;
        newCodeLine = null;
        PickNewLine();
        InsertNewLine();
        MoveLinesUp();
    }

    void PickNewLine()
    {
        tmpRnd = Random.Range(0, 100);
        Lebug.Log("tmpRnd", tmpRnd);
        if (lineCount > 0 && prevCLT == CodeLineType.Parentheses) // Previous line is if
        {
            newCLT = CodeLineType.CurlyBracesOpen;

        } else if (tmpRnd < 30 && indentationLevel < 2 && prevCLT != CodeLineType.Parentheses)
        {
            // We do not want more than 2 levels of indentation
            newCLT = CodeLineType.Parentheses; // if
        }
        else if (tmpRnd > 75 && indentationLevel > 0 && prevCLT != CodeLineType.CurlyBracesOpen)
        {
            // Nadiren (%20), indentation varsa, ve daha yeni açılmadıysa kapayabiliriz 
            newCLT = CodeLineType.CurlyBracesClose;
        }
        else
        {
            // generally normal lines but also normal line if there is no indentation as the next option is to close 
            newCLT = CodeLineType.Semicolon;
        }

    }

    void InsertNewLine()
    {
        switch (newCLT)
        {
            case CodeLineType.Parentheses:
                newCodeLine = Instantiate(clParentheses);
                break;
            case CodeLineType.CurlyBracesOpen:
                newCodeLine = Instantiate(clCurlyOpen);
                indentationLevel++;
                break;
            case CodeLineType.CurlyBracesClose:
                newCodeLine = Instantiate(clCurlyClose);
                indentationLevel--;
                break;
            case CodeLineType.Semicolon:
                newCodeLine = Instantiate(clSemicolon);
                break;
            default:
                break;
        }

        //Debug.Log("Instantiated");
        newCodeLine.transform.parent = CodeWrapper.transform;
        newCodeLine.transform.position = InstantiatePosition;
        newCodeLine.transform.SetPosX(newCodeLine.transform.localPosition.x + (indentationLevel * indentationAmount));

        tmpRnd = Random.Range(0, 100);
        if (tmpRnd < 20) // TODO düzelt, 10
        {
            newCodeLine.SetBug(true);
        } else
        {
            newCodeLine.SetBug(false);
        }

        lineCount++;

    }

    #endregion

    void MoveLinesUp()
    {
        Vector3 pos = CodeWrapper.transform.position;
        pos.y += lineHeight;
        Lebug.Log("lineCount", lineCount);
        Lebug.Log("pos", pos);
        CodeWrapper.transform.position = pos;
    }

    void CheckCodeClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            //Debug.Log("hit");
            //Debug.Log(hit.transform.name);
            if (hit != null && hit.collider != null)
            {
                if (hit.transform.GetComponent<CodeLine>() != null)
                {
                    //Debug.Log("Click to code line");
                    CodeLine cl = hit.transform.GetComponent<CodeLine>();
                    if (cl.IsLineBuggy)
                    {
                        if (selectedCodeLine != null)
                        {
                            selectedCodeLine.RemoveCursor();
                        }
                        selectedCodeLine = cl;
                        cl.AddCursor();
                    }
                }
                else if (hit.transform.GetComponent<LineEnder>() != null)
                {
                    //Debug.Log("Click to LineEnder");
                    LineEnder le = hit.transform.GetComponent<LineEnder>();
                    if (selectedCodeLine != null)
                    {
                        selectedCodeLine.AddLineEnder(le.CodeLineType);
                    }
                }
            } 
        }
    }

    public void InitializeBug()
    {
        tmpRnd = Random.Range(0, BugTypeCount);
        eventManager.softwareBugEvent.Invoke((BugEnums)tmpRnd);
    }

    public void BugFixedCompiled()
    {
        eventManager.fixSoftwareEvent.Invoke();
    }

    public void HandleCodeLineCompiled(CodeLine cl)
    {
        if (selectedCodeLine == cl)
            ResetSelectedCodeLine();
    }

    public void ResetSelectedCodeLine()
    {
        selectedCodeLine = null;
    }

    public Sprite GetRandomCodeLineSprite()
    {
        tmpRnd = Random.Range(0, codeLineBaseSprites.Count);
        return codeLineBaseSprites[tmpRnd];
    }

    public Sprite GetSpriteByCLT(CodeLineType _CLT)
    {
        Sprite sp;
        switch (_CLT)
        {
            case CodeLineType.Parentheses:
                sp = spParenthesesClose;
                break;
            case CodeLineType.CurlyBracesOpen:
                sp = spCurlyOpen;
                break;
            case CodeLineType.CurlyBracesClose:
                sp = spCurlyClose;
                break;
            case CodeLineType.Semicolon:
                sp = spSemicolon;
                break;
            default:
                Debug.LogError("Unhandled CodeLineType : " + _CLT);
                sp = spParenthesesClose;
                break;
        }
        return sp;
    }
}
