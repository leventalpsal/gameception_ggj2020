﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor : MonoBehaviour
{

    bool display = true;
    [SerializeField] SpriteRenderer cursorSr;
    [SerializeField] float blinkTime;


    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("ToggleDisplay", blinkTime, blinkTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ToggleDisplay()
    {
        display = !display;
        cursorSr.enabled = display;
    }
}
