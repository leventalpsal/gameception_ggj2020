﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour
{
    public GameObject rightBorder;
    public GameObject leftBorder;
    public GameObject downBorder;
    public EnemiesManager enemiesManager;
    public GameObject gameOverScreen;

    public Vector3 startPosition;

    private bool enemyBug;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > rightBorder.transform.position.x)
        {
            enemiesManager.goRight = false;
            enemiesManager.goLeft = true;
            enemiesManager.goDown = true;
        }
        if (transform.position.x < leftBorder.transform.position.x)
        {
            enemiesManager.goLeft = false;
            enemiesManager.goRight = true;
            enemiesManager.goDown = true;
        }
        if (transform.position.y < downBorder.transform.position.y)
        {
            gameOverScreen.SetActive(true);
            StartCoroutine(EndGame(2f));
        }

        Movement();  
    }

    private IEnumerator EndGame(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            SceneManager.LoadScene("MainMenu");
        }
    }

    public void GoDown()
    {
        transform.Translate(Vector2.down / 10);
    }

    public void Movement()
    {
        if (enemiesManager.goRight)
        {
            if(enemyBug)
                transform.Translate(Vector2.right * Time.deltaTime * 4);
            else
                transform.Translate(Vector2.right * Time.deltaTime * 2);
        }

        if (enemiesManager.goLeft)
        {
            if(enemyBug)
                transform.Translate(Vector2.left * Time.deltaTime * 4);
            else
                transform.Translate(Vector2.left * Time.deltaTime * 2);
        }
            
    }

    public void Bugs(BugEnums bugEnum)
    {
        if (bugEnum == BugEnums.FasterEnemies)
            enemyBug = true;
    }

    public void Fixes(BugEnums bugEnum)
    {
        if (bugEnum == BugEnums.FasterEnemies)
            enemyBug = false;
    }
}
