﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketController : MonoBehaviour
{
    private GameObject upBorder;

    // Start is called before the first frame update
    void Start()
    {
        upBorder = GameObject.Find("ScreenBordersUp");
    }

    // Update is called once per frame
    void Update()
    {
        Movement();

        if (transform.position.y > upBorder.transform.position.y)
            Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "SpaceInvadersEnemy")
        {
            col.gameObject.transform.position = col.gameObject.GetComponent<EnemyController>().startPosition;
            FindObjectOfType<EnemiesManager>().deadEnemies.Add(col.gameObject);
            col.gameObject.SetActive(false);
            FindObjectOfType<ScoreCounter>().AddScore();
            Destroy(gameObject);
        }
    }

    public void Movement()
    {
        transform.Translate(Vector2.up * Time.deltaTime * 4);
    }
}
