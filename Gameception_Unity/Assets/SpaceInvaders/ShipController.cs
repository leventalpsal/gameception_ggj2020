﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    public GameObject leftBorder;
    public GameObject rightBorder;

    public bool fireBug;
    public bool movementBug;
    public bool firecolliderBug;

    public Object rocket;

    private float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        Fire();
    }

    public void Movement()
    {
        if (Input.GetAxis("Horizontal") > 0.25)
        {
            if (movementBug)
            {
                if (transform.position.x > leftBorder.transform.position.x)
                {
                    transform.Translate(Vector2.left * Time.deltaTime * 2);
                }
            }
            else
            {
                if (transform.position.x < rightBorder.transform.position.x)
                {
                    transform.Translate(Vector2.right * Time.deltaTime * 2);
                }
            }
        }
        else if (Input.GetAxis("Horizontal") < -0.25)
        {
            if (movementBug)
            {
                if (transform.position.x < rightBorder.transform.position.x)
                {
                    transform.Translate(Vector2.right * Time.deltaTime * 2);
                }
            }
            else
            {
                if (transform.position.x > leftBorder.transform.position.x)
                {
                    transform.Translate(Vector2.left * Time.deltaTime * 2);
                }
            }
        }
    }

    public void Bugs(BugEnums bugEnum)
    {
        if (bugEnum == BugEnums.WrongFire)
            fireBug = true;
        else if (bugEnum == BugEnums.UselessFire)
            firecolliderBug = true;
        else if (bugEnum == BugEnums.Movement)
            movementBug = true;
    }
    public void Fixes(BugEnums bugEnum)
    {
        if (bugEnum == BugEnums.WrongFire)
            fireBug = false;
        else if (bugEnum == BugEnums.UselessFire)
            firecolliderBug = false;
        else if (bugEnum == BugEnums.Movement)
            movementBug = false;
    }

    public void Fire()
    {
        timer += Time.deltaTime;
        if (Input.GetButton("Jump"))
        {
            if (timer > 1f)
            {
                if (fireBug)
                    Instantiate(rocket, new Vector3(transform.position.x, transform.position.y + 0.3f, transform.position.z), Quaternion.Euler(0f, 0f, 45f));
                else if (firecolliderBug)
                {
                    GameObject tempRocket = Instantiate(rocket, new Vector3(transform.position.x, transform.position.y + 0.3f, transform.position.z), Quaternion.identity) as GameObject;
                    tempRocket.GetComponent<Collider2D>().enabled = false;
                }
                else
                    Instantiate(rocket, new Vector3(transform.position.x, transform.position.y + 0.3f, transform.position.z), Quaternion.identity);

                timer = 0f;

                GetComponent<AudioSource>().Play();
            }
        }
    }
}
