﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesManager : MonoBehaviour
{
    public bool goLeft = true;
    public bool goRight = false;
    public bool goDown = false;

    public Object enemiesPrefab;

    public List<GameObject> deadEnemies = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (goDown)
        {
            EnemyController[] enemies = GetComponentsInChildren<EnemyController>();
            for (int i=0; i<enemies.Length; i++)
            {
                enemies[i].GoDown();
            }
            goDown = false;
        }

        if (null == GetComponentInChildren<EnemyController>())
        {
            for (int i=0; i<deadEnemies.Count; i++)
            {
                deadEnemies[i].SetActive(true);
            }

        }
    }
}
