﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneOpener : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(OpenMenu(18.0f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator OpenMenu(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            SceneManager.LoadScene("MainMenu");
        }
    }
}
