﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugSender : MonoBehaviour
{
    [SerializeField] private GameObject[] _bugSender;
    [SerializeField] private GameObject[] _companent;

    List<SpriteRenderer> activeMechanic = new List<SpriteRenderer>();
    List<BugEnums> bugs = new List<BugEnums>();
    List<BugEnums> activeBugs = new List<BugEnums>();

    void Start()
    {
        StartCoroutine(BugZsender());
        bugs.Add(BugEnums.Dithering);
        bugs.Add(BugEnums.FasterEnemies);
        bugs.Add(BugEnums.Movement);
        bugs.Add(BugEnums.Pixelation);
        bugs.Add(BugEnums.UselessFire);
        bugs.Add(BugEnums.WrongFire);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator BugZsender()
    {
        while (true)
        {
            yield return new WaitForSeconds(20.0f);
            int rand = Random.Range(0, 7);
            _bugSender[rand].GetComponent<SpriteRenderer>().enabled = true;
            activeMechanic.Add(_bugSender[rand].GetComponent<SpriteRenderer>());
            int randBug = Random.Range(0,5);
            FindObjectOfType<EventManager>().hardwareBugEvent.Invoke(bugs[randBug]);
        }
       
    }

    public void FixBug()
    {
        activeMechanic[0].enabled = false;
        FindObjectOfType<EventManager>().fixHardwareEvent.Invoke();
    }
}
