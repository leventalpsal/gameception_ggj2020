﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(BoxCollider2D))]
public class Devre : MonoBehaviour
{
    private bool _isPlaced, onMotherBoard; // companentin yerine oturup oturmaması ile ilgili boolen
    [SerializeField] private Transform _companentPlace1;
    [SerializeField] private Transform _companentPlace2;
    private Plane dragPlane;
    private Vector3 offset;

    private Camera myMainCamera;
    void Start()
    {
        myMainCamera = Camera.main;
        _isPlaced = false;
       
    }
    
    private void Update()
    {
        transform.Rotate(new Vector3(0,0,10) * 400 * Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime); //mouse scroll
        if (Vector3.Distance(transform.position, _companentPlace1.position) < 2.00f && Quaternion.Angle(_companentPlace1.rotation, transform.rotation) < 20.0f && Input.GetMouseButtonUp(0))
        {
            print("Yeri ve Açısı Doğru ve Mouse Bırakıldı");
            _isPlaced = true;
            FindObjectOfType<BugSender>().FixBug();
            Destroy(gameObject);
        }

        if (Vector3.Distance(transform.position, _companentPlace2.position) < 2.00f && Quaternion.Angle(_companentPlace2.rotation, transform.rotation) < 20.0f && Input.GetMouseButtonUp(0))
        {
            print("Yeri ve Açısı Doğru ve Mouse Bırakıldı");
            _isPlaced = true;
            FindObjectOfType<BugSender>().FixBug();
            Destroy(gameObject);
        }

        if (Input.GetMouseButtonUp(0)&&_isPlaced==false)
        {
           
            print("Yeri Hatalı...");
        }

        
        
       
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        //if (other.gameObject.name == "MotherBoard")
        //{
        //    onMotherBoard = true;
        //}
        //else
        //{
        //    onMotherBoard = false;
        //}
    }
    
    protected void OnMouseEnter()
    {
        dragPlane = new Plane(myMainCamera.transform.forward, transform.position);
        Ray camRay = myMainCamera.ScreenPointToRay(Input.mousePosition);

        float planeDist;
        dragPlane.Raycast(camRay, out planeDist);
        offset = transform.position - camRay.GetPoint(planeDist);
    }

   protected void OnMouseDrag()
    {
        Ray camRay = myMainCamera.ScreenPointToRay(Input.mousePosition);

        float planeDist;
        dragPlane.Raycast(camRay, out planeDist);
        transform.position = camRay.GetPoint(planeDist) + offset;
    }

}

